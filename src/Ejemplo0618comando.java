/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Fichero: Ejemplo0618comando.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-ene-2014
 */
public class Ejemplo0618comando {

  public static void main(String args[]) {
    String s = null;
    try {
      // Determinar en que SO estamos
      String so = System.getProperty("os.name");
      String comando;
      // Comando para Linux
      if (so.equals("Linux")) {
        comando = "ifconfig";
      } // Comando para Windows
      else {
        comando = "cmd /c ipconfig";
      }
      // Ejcutamos el comando
      Process p = Runtime.getRuntime().exec(comando);
      BufferedReader stdInput =
              new BufferedReader(new InputStreamReader(p.getInputStream()));
      BufferedReader stdError =
              new BufferedReader(new InputStreamReader(p.getErrorStream()));
      // Leemos la salida del comando
      System.out.println("Esta es la salida standard del comando:\n");
      while ((s = stdInput.readLine()) != null) {
        System.out.println(s);
      }
      // Leemos los errores si los hubiera
      System.out.println("Esta es la salida standard de error.\n");
      while ((s = stdError.readLine()) != null) {
        System.out.println(s);
      }
      System.exit(0);
    } catch (IOException e) {
      System.out.println("Excepcion: ");
      e.printStackTrace();
      System.exit(-1);
    }
  }
}
/* EJECUCION:
 Esta es la salida standard del comando:
 eth1      Link encap:Ethernet  direccionHW 00:19:66:bb:6e:60
 ...
 */
