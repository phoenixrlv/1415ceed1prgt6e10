/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//http://3kbzotas.wordpress.com/2011/08/01/creando-xml-en-java-de-la-forma-mas-facil-rapida-e-intuitiva/

import com.sun.org.apache.xml.internal.serializer.OutputPropertiesFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 * Fichero: Ejemplo21XMLDomWrite.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-ene-2014
 */
class Ejemplo21XMLDomWrite {

  public static void main(String[] args) throws Exception {

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    DOMImplementation implementation = builder.getDOMImplementation();

    // Creamos el nuevo documento XML
    Document document;
    document = implementation.createDocument(null, "articulos", null);
    document.setXmlVersion("1.0");
    Element raiz = document.getDocumentElement();

    // Articulo 1
    Element articulo = document.createElement("articulo"); //creamos un nuevo elemento
    articulo.setAttribute("id", "1");

    // Nombre Articulo 1
    Element nombre = document.createElement("nombre");
    Text valorNombre = document.createTextNode("Paco");
    articulo.appendChild(nombre);
    nombre.appendChild(valorNombre);

    // Precio Articulo 1
    Element precio = document.createElement("precio");
    Text valorPrecio = document.createTextNode("12"); //Ingresamos la info	
    articulo.appendChild(precio);
    precio.appendChild(valorPrecio);

    // Añadimos Articulo 1
    raiz.appendChild(articulo); //pegamos el elemento a la raiz "Documento"

    // Articulo 2
    Element articulo2 = document.createElement("articulo"); //creamos un nuevo elemento
    articulo2.setAttribute("id", "2");

    // Nombre Articulo 2
    Element nombre2 = document.createElement("nombre");
    Text valorNombre2 = document.createTextNode("Juan"); //Ingresamos la info				
    articulo2.appendChild(nombre2);
    nombre2.appendChild(valorNombre2);

    // Precio Articulo 2
    Element precio2 = document.createElement("precio");
    Text valorPrecio2 = document.createTextNode("23"); //Ingresamos la info	
    articulo2.appendChild(precio2);
    precio2.appendChild(valorPrecio2);

    // Añadimos articulo 2
    raiz.appendChild(articulo2); //pegamos el elemento a la raiz "Documento"

    Source source = new DOMSource(document);
    Result result = new StreamResult(new java.io.File("Ejemplo21XMLDoom.xml")); //nombre del archivo

    Transformer transformer = TransformerFactory.newInstance().newTransformer();
    //Generar el tranformador para obtener el documento XML en un fichero
    //Insertar saltos de línea al final de cada línea
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    //Añadir 3 espacios delante, en función del nivel de cada nodo
    transformer.setOutputProperty(OutputPropertiesFactory.S_KEY_INDENT_AMOUNT, "3");
    transformer.transform(source, result);
  }
}
/* EJECUCIÓN: 
 * Crea  el fichero Ejemplo21XMLDoom.xml en el direct. raiz del proyecto con:
 <?xml version="1.0" encoding="UTF-8" standalone="no"?>
 <articulos>
 <articulo id="1">
 <nombre>Paco</nombre>
 <precio>12</precio>
 </articulo>
 <articulo id="2">
 <nombre>Juan</nombre>
 <precio>23</precio>
 </articulo>
 </articulos>
 */
