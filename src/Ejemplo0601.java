/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.StringReader;

/**
 * Fichero: Ejemplo0601.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-ene-2014
 */
public class Ejemplo0601 {

  public static void main(String[] args) {
    String string = "Probando 1 2 3...";
    StringReader stringReader = new StringReader(string);
    int car; // Caracter lee de Reader
    try {
      while ((car = stringReader.read()) != -1) {
        System.out.print((char) car);
      }
      System.out.println("");
    } catch (IOException ioe) {
    }
  }
}
/* EJECUCION:
 Probando 1 2 3...
 */
