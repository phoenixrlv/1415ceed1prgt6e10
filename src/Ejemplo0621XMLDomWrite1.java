/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//http://3kbzotas.wordpress.com/2011/08/01/creando-xml-en-java-de-la-forma-mas-facil-rapida-e-intuitiva/

import com.sun.org.apache.xml.internal.serializer.OutputPropertiesFactory;
import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 * Fichero: Ejemplo21XMLDomWrite.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-ene-2014
 */
class Ejemplo21XMLDomWrite1 {

  static final String FILENAME = "Ejemplo21XMLDoom.xml";

 
  public static Document creaDocument(String tagRaiz)
          throws ParserConfigurationException {

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    DOMImplementation implementation = builder.getDOMImplementation();
    // Creamos el nuevo documento XML
    Document document;
    document = implementation.createDocument(null, tagRaiz, null);
    return document;

  }

  private static void creaArayList(ArrayList<ArticuloEj21> arrayList) {

    arrayList.add(new ArticuloEj21("1", "Paco", 12));
    arrayList.add(new ArticuloEj21("2", "Juan", 23));

  }

  /*
   * Crea un nodo articulo
   */
  public static Element creaNodoArticulo(Document doc, ArticuloEj21 art) {
    // Articulo 
    Element articulo = doc.createElement("articulo"); //creamos un nuevo elemento
    articulo.setAttribute("id", art.getId());

    // Nombre Articulo 
    Element nombre = doc.createElement("nombre");
    Text valorNombre = doc.createTextNode(art.getNombre());
    articulo.appendChild(nombre);
    nombre.appendChild(valorNombre);

    // Precio Articulo 
    Element precio = doc.createElement("precio");
    int prec = art.getPrecio();
    Text valorPrecio = doc.createTextNode(Integer.toString(prec)); //Ingresamos la info	
    articulo.appendChild(precio);
    precio.appendChild(valorPrecio);

    return articulo;

  }
  
  // This method writes a DOM doc to a file
  public static void writeXmlFile(Document doc, String filename) {
    try {
      // Prepare the DOM doc for writing
      Source source = new DOMSource(doc);
      // Prepare the output file
      File file = new File(filename);
      Result result = new StreamResult(FILENAME);
      Transformer transformer = TransformerFactory.newInstance().newTransformer();
      //Generar el tranformador para obtener el documento XML en un fichero
      //Insertar saltos de línea al final de cada línea
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      //Añadir 3 espacios delante, en función del nivel de cada nodo
      transformer.setOutputProperty(OutputPropertiesFactory.S_KEY_INDENT_AMOUNT, "3");
      transformer.transform(source, result);
    } catch (TransformerConfigurationException e) {
    } catch (TransformerException e) {
    }
  }


  public static void main(String[] args) throws Exception {

    Element articulo;

    ArrayList<ArticuloEj21> arrayList = new ArrayList<>();
    creaArayList(arrayList);

    Document doc = creaDocument("articulos");
    doc.setXmlVersion("1.0");
    Element raiz = doc.getDocumentElement();

    //Recorre la lista e inserta en el doc xml un objeto articulo
    for (ArticuloEj21 a : arrayList) {
      articulo = creaNodoArticulo(doc, a);
      raiz.appendChild(articulo); //pegamos el elemento a la raiz "Documento"
    }

    // Graba fichero xml
    writeXmlFile(doc, FILENAME);
  }
}

class ArticuloEj21 {

  String id;
  String nombre;
  int precio;

  ArticuloEj21(String id_, String nombre_, int precio_) {
    id = id_;
    nombre = nombre_;
    precio = precio_;
  }

  String getId() {
    return id;
  }

  String getNombre() {
    return nombre;
  }

  int getPrecio() {
    return precio;
  }
}

/* EJECUCIÓN: 
 * Crea  el fichero Ejemplo21XMLDoom.xml en el direct. 
 * raiz del proyecto con:
 
 <?xml version="1.0" encoding="UTF-8" standalone="no"?>
 <articulos>
 
 <articulo id="1">
 <nombre>Paco</nombre>
 <precio>12</precio>
 </articulo>
 
 <articulo id="2">
 <nombre>Juan</nombre>
 <precio>23</precio>
 
 </articulo>
 
 </articulos>
 */
