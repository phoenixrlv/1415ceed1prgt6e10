/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;

/**
 * Fichero: Ejemplo0609PushbackReader.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-ene-2014
 */
public class Ejemplo0609PushbackReader {

  public static void main(String args[])
          throws IOException {
    String sentencias = "a=a+1;c++;b+=5;c=a+b;b++;";
    StringReader sr = new StringReader(sentencias);
    PushbackReader pbr = new PushbackReader(sr);
    int ultimo = pbr.read();
    int penultimo = 0;
    while (ultimo != -1) {
      switch (ultimo) {
        case '+':
          ultimo = pbr.read();
          if (ultimo == '+') {
            System.out.print("=" + (char) penultimo + "+1");
          } else {
            pbr.unread(ultimo);
            System.out.print("+");
          }
          break;
        case ';':
          System.out.println((char) ultimo);
          break;
        default:
          System.out.print((char) ultimo);
      }
      penultimo = ultimo;
      ultimo = pbr.read();
    } // while
  } // main
} // class
/* EJECUCION
 a=a+1;
 c=c+1;
 b+=5;
 c=a+b;
 b=b+1;
 */
