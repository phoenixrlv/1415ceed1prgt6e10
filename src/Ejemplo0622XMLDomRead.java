/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Fichero: Ejemplo22XMLDomRead.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-ene-2014
 */
public class Ejemplo0622XMLDomRead {

  public static void main(String[] args) throws Exception {

    //Creamos DOM Builder Factory
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    DocumentBuilder db = dbf.newDocumentBuilder();
    Document document = db.parse(new File("Ejemplo21XMLDoom.xml"));


    List<Articulo> artList = new ArrayList<>();

    //Iterando a través de los nodos obtenemos los datos.
    NodeList nodeList = document.getDocumentElement().getChildNodes();

    for (int i = 0; i < nodeList.getLength(); i++) {
      //Hemos encontrado un  <articulo> tag.
      Node node = nodeList.item(i);
      if (node instanceof Element) {
        Articulo art = new Articulo();

        art.id = node.getAttributes().getNamedItem("id").getNodeValue();

        NodeList childNodes = node.getChildNodes();
        for (int j = 0; j < childNodes.getLength(); j++) {
          Node cNode = childNodes.item(j);
          //Identificando la etiquete hija de articulo encontrado.
          if (cNode instanceof Element) {

            String content = cNode.getLastChild().
                    getTextContent().trim();

            switch (cNode.getNodeName()) {
              case "nombre":
                art.nombre = content;
                break;
              case "precio":
                art.precio = content;
                break;
            }
          }
        }
        artList.add(art);
      }
    }
    //Printing the Articulo list populated.
    for (Articulo art : artList) {
      System.out.println(art);
    }

  }
}

class Articulo {

  String id;
  String nombre;
  String precio;

  public String toString() {
    return id + " " + nombre + " " + " " + precio;
  }
}
/* EJECUCION:
 1 Paco  12
 2 Juan  23
 * */
