/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Fichero: Ejemplo0610.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-ene-2014
 */
public class Ejemplo0610 {

  public static void main(String args[]) {
    FileOutputStream f = null;
    String s = "texto";
    char c = 0;
    try {
      f = new FileOutputStream("datos.bin");
      for (int i = 0; i < s.length(); i++) {
        c = s.charAt(i);
        f.write((byte) c);
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        System.out.println("Texto grabado");
        f.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
/* EJECUCION
 Texto grabado
 */
