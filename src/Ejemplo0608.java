/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;

/**
 * Fichero: Ejemplo0608.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-ene-2014
 */
public class Ejemplo0608 {

  public static void main(String args[])
          throws IOException {
    StringReader sr = new StringReader("abc");
    PushbackReader pbr = new PushbackReader(sr);
    int c = pbr.read();
    pbr.unread(c);
    while (c != -1) {
      System.out.print((char) c);
      c = pbr.read();
    } // while
  } // main
} // class
/* EJECUCION
 aabc
 */
