
import java.io.File;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Fichero: Ejemplo0613File.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-ene-2014
 */
public class Ejemplo0613File {

  public static void main(String args[]) {
    String nomdir = "/home/paco/";
    File dir = new File(nomdir);
    if (dir.exists()) {
      System.out.println("Existe diretorio " + dir.getName());
    } else {
      System.out.println("No Existe diretorio " + dir.getName());
    }
    if (dir.canRead()) {
      System.out.println("Tiene permiso lectura " + dir.getName());
    } else {
      System.out.println("No Tiene permiso lectura " + dir.getName());
    }
    File[] ficheros = dir.listFiles();
    System.out.println("El contenido de " + nomdir + " es: ");
    for (File f : ficheros) {
      System.out.println(f.getName());
    }
  }
}
/* EJECUCION:
 Existe diretorio prueba
 Tiene permiso lectura prueba
 El contenido de /home/paco/prueba es: 
 fichero1.txt
 */
