/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * Fichero: Ejemplo0614Html.java
 * @date 30-ene-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejemplo0614Html {

  public static void main (String args[]) {
  String cad = "Ejemplo0614Html";
       
 FileWriter filewriter = null;
 PrintWriter printw = null;
       
 try{
     filewriter = new FileWriter("Ejemplo0607Html.html");//declarar el archivo
     printw = new PrintWriter(filewriter);//declarar un impresor
           
     printw.println("<html>");
     printw.println("<head><title>Ejemplo0614Html</title></head>");    
     //si queremos escribir una comilla " en el
     //archivo uzamos la diagonal invertida \"
     printw.println("<body bgcolor=\"#99CC99\">");
    
     //si quisieramos escribir una cadena que vide de una lista o
     //de una variable lo concatenamos
     printw.println("<center><h1><font color=\"navy\">"+cad+"</font></h1></center>");
     printw.println("<center><h4><font color=\"purple\">Ejemplo de FileWriter</font></h4></center>");
    
 
     printw.println("</body>");
     printw.println("</html>");
           
     //no devemos olvidar cerrar el archivo para que su lectura sea correcta
     printw.close();//cerramos el archivo
 }catch (Exception e){}      
     System.out.println("Generada página web");//si todo sale bien mostramos un mensaje de guardado exitoso
  }
}
/* EJECUCION
Generada página web
*/
