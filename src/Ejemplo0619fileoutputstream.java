/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;

/**
 * Fichero: Ejemplo0619fileoutputstream.java 
 * @author Paco Aldarias <paco.aldarias@ceedcv.es> 
 * @date 29-ene-2014 
 */



public class Ejemplo0619fileoutputstream {
/**
 * Progama que guarda en un archivo nombre y alturas
 * Despues las lee y las muestra por pantalla
 * utilizando tipos primitivos
  */




  public static void main(String[] args)  {
    String[] amigos= {"Pepe", "Juan"};
    double [] altura= { 177.5, 170.25};
    // escribe los datos
    try {
      FileOutputStream fs = new FileOutputStream("amigos.bin");
      DataOutputStream d=new DataOutputStream(fs);
      for (int i=0; i<amigos.length; i++) {
        d.writeUTF(amigos[i]);
        d.writeDouble(altura[i]);
      }
      if ( d != null ) {
        fs.close();
        d.close();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    //leer los datos del archivo
    String s;
    double a;
    double total=0.0;
    try { // try1
     // Se ponen dos try uno para abrir fichero y otro para leer datos
      File f=null;
      FileInputStream fe = null;
      DataInputStream d = null;
      try { // try2
        f = new File("amigos.bin");
        if ( f.exists() ) {
          fe = new FileInputStream(f);
          d = new DataInputStream(fe);
          while(true) {
            s=d.readUTF();
            System.out.print(s+ "  -> ");
            a = d.readDouble();
            System.out.println(a);
            total=total+a;
          }
        
        }
      } catch (EOFException e) { // try2
        System.out.println("-------------------");
        total = total/amigos.length;
        System.out.println("Media -> "+total);
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      } catch (Throwable e) {
        e.printStackTrace();
      } finally { // fin try2
        if ( d !=null ) {
          d.close();
          fe.close();
        }
      }
      
    } catch (IOException e) { // try1
      System.err.println("IOException");
      e.printStackTrace();
    } // fin try1
  }

}
/* EJECUCION:
Pepe  -> 177.5
Juan  -> 170.25
-------------------
Media -> 173.875
*/