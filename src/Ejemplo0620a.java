
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * @date 27-ene-2015 Fichero Ejemplo0620a.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class Persona implements Serializable {

    private String nombre;

    Persona(String nombre_) {
        nombre = nombre_;
    }

    String getNombre() {
        return nombre;
    }
}

public class Ejemplo0620a {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        // TODO code application logic here

        Persona p1;
        FileOutputStream fos = new FileOutputStream("f.bin");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        p1 = new Persona("Paco");
        oos.writeObject(p1);
        oos.close();

        Persona p2 = null;
        FileInputStream fis = new FileInputStream("f.bin");
        ObjectInputStream ois = new ObjectInputStream(fis);
        p2 = (Persona) ois.readObject();
        System.out.println(p2.getNombre());
        ois.close();

    }

}
