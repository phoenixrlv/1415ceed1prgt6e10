/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Fichero: Ejemplo0615FileReader.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-ene-2014
 */
public class Ejemplo0615FileReader {

  /* Programa que cuenta cuantas
   * palabras contiene un archivo
   * El archivo se pasa como parametro.
   */
  public int cuentaPalabras(String fichero) {
    int contador = 0;
    try {
      String s;
      File fe = new File(fichero);
      FileReader fr = new FileReader(fe);
      BufferedReader f = new BufferedReader(fr);
      while ((s = f.readLine()) != null) {
        StringTokenizer str = new StringTokenizer(s);
        contador += str.countTokens();
      }
      f.close();
    } catch (FileNotFoundException e) {
      System.out.println("Error: No existe " + fichero);
    } catch (IOException e) {
    } catch (Throwable e) {
    }
    return contador;
  }

  public static void main(String[] args) {
    Ejemplo0615FileReader c = new Ejemplo0615FileReader();
    int d = c.cuentaPalabras(args[0]);
    if (d != 0) {
      System.out.println("Fichero: " + args[0]);
      System.out.println("Palabras: " + d);
    }
  }
}
/* EJECUCION:
 Fichero: filereader01.txt
 Palabras: 16
 */
